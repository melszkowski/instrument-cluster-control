
#ifndef PSTOOLS_H_INCLUDED
#define PSTOOLS_H_INCLUDED

#include <iostream>
#include <string>
#include <windows.h>
#include <tlhelp32.h>
#include <psapi.h>


struct handle_data
{
        unsigned long processId;
        HWND hBestHandle;
};
BOOL CALLBACK EnumWindowsCallback(HWND handle, LPARAM lParam);
BOOL IsMainWindow(HWND handle);


class ProcessTools
{
    HANDLE hSnap;
    PROCESSENTRY32 pe;

public:
    ProcessTools()
    {
        hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        if(hSnap != INVALID_HANDLE_VALUE)
        {
            ZeroMemory(&pe, sizeof(PROCESSENTRY32));
            pe.dwSize = sizeof(PROCESSENTRY32);
        }
    }
    ~ProcessTools()
    {
        CloseHandle(hSnap);
    }
    HANDLE OpenProcess(LPCSTR lpcsName, DWORD dwAccess)
    {
        if(Process32First(hSnap, &pe))
        {
            do
            {
                if(!lstrcmpi(pe.szExeFile, lpcsName))
                {
                    return ::OpenProcess(dwAccess, NULL, pe.th32ProcessID);
                }
            } while (Process32Next(hSnap, &pe));
        }
        return 0;
    }
    DWORD GetProcessId(LPCSTR lpcsName)
    {
        if(Process32First(hSnap, &pe))
        {
            do
            {
                if(!lstrcmpi(pe.szExeFile, lpcsName))
                {
                    return pe.th32ProcessID;
                }
            } while (Process32Next(hSnap, &pe));
        }
        return 0;
    }
    DWORD GetProcessId(LPCSTR lpcsClassName, LPCSTR lpcsWindowName)
    {
        DWORD* lpdwProcessId = NULL;

        HWND hWnd = FindWindow(lpcsClassName, lpcsWindowName);
        if(hWnd != INVALID_HANDLE_VALUE)
        {
            GetWindowThreadProcessId(hWnd, lpdwProcessId);
        }
        return 0;
    }
    HWND FindWindowByPid(unsigned long processId)
    {
        handle_data data;
        data.processId = processId;
        data.hBestHandle = 0;

        EnumWindows(EnumWindowsCallback, (LPARAM)&data);
        return data.hBestHandle;
    }
    void GetWindowTitle(int processId, LPTSTR lpString, int nMaxCount)
    {
        HWND hWnd = FindWindowByPid(processId);
        GetWindowText(hWnd, lpString, nMaxCount);
        //GetWindowText(FindWindowByPid(processId), lpString, nMaxCount);
    }
    DWORD GetProcessBaseAddress(DWORD processId)
    {
        MODULEENTRY32 me32 = { sizeof( MODULEENTRY32 ) };
        HANDLE hSnapshot = CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, processId);

        if (hSnapshot == INVALID_HANDLE_VALUE)
            return 0;

        if (Module32First(hSnapshot, &me32))
        {
            CloseHandle( hSnapshot );
            return (DWORD)me32.modBaseAddr;
        }

        CloseHandle(hSnapshot);
        return 0;
    }
    DWORD GetModuleBase(HANDLE hProc, std::string &sModuleName)
    {
       HMODULE *hModules;
       char szBuf[256];
       DWORD cModules;
       DWORD dwBase = -1;
       //------

       EnumProcessModules(hProc, hModules, 0, &cModules);
       hModules = new HMODULE[cModules/sizeof(HMODULE)];

       if(EnumProcessModules(hProc, hModules, cModules/sizeof(HMODULE), &cModules)) {
          for(int i = 0; i < cModules/sizeof(HMODULE); i++) {
             if(GetModuleBaseName(hProc, hModules[i], szBuf, sizeof(szBuf))) {
                if(sModuleName.compare(szBuf) == 0) {
                   dwBase = (DWORD)hModules[i];
                   break;
                }
             }
          }
       }
       delete[] hModules;

       return dwBase;
    }
};

#endif // PSTOOLS_H_INCLUDED
