#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <typeinfo>
#include "pstools.h"

#define BAUD_RATE 9600
using namespace std;

class NFSUG2
{
    ProcessTools ps;
    HANDLE hProc;

    string exeName = "SPEED2.EXE";
    static const int moneyAddr = 0x461e74;
    static const int speedAddr = 0x3f09e8;
    static const int engineSpeedAddr = 0x105C6D98;
    //static const int nitroAddr = 0x1613030C;

    int baseAddr = 0;

    int money = 0;
    float speed = 0.0;
    float engineSpeed = 0;
    //int nitroAmount = 0;

public:
    NFSUG2()
    {
        hProc = ps.OpenProcess(exeName.c_str(), PROCESS_ALL_ACCESS);
        baseAddr = ps.GetProcessBaseAddress(ps.GetProcessId(exeName.c_str()));
    }
    void PrintInfo()
    {
        cout << "hProc: " << hex << hProc << endl;
        cout << "baseAddres: 0x" << hex << baseAddr << endl;
    }
    int ReadMoney()
    {
        ReadProcessMemory(hProc, (LPVOID)(baseAddr + moneyAddr), &money, sizeof(int), NULL);
        return money;
    }
    float ReadSpeed()
    {
        ReadProcessMemory(hProc, (LPVOID)(baseAddr + speedAddr), &speed, sizeof(float), NULL);
        return speed;
    }
    float ReadEngineSpeed()
    {
        ReadProcessMemory(hProc, (LPVOID)(baseAddr + engineSpeedAddr), &engineSpeed, sizeof(float), NULL);
        return engineSpeed;
    }
  // int ReadNitroAmount()
    //{
//
     //   ReadProcessMemory(hProc, (LPVOID)(baseAddr + nitroAddr), &nitroAmount, sizeof(int), NULL);
     //   return nitroAmount;
    //}

};

class SerialPort
{
    DCB dcbControl;
    HANDLE hPortHandle = NULL;
    char lpReadBuffer[16] = {0};
    byte lpWriteBuffer[16] = {0};
    DWORD dwBaudRate = 9600;
private:

public:
    bool OpenPort(char* lpPortName, DWORD dwAttr)
    {
        hPortHandle = CreateFile(lpPortName, dwAttr, 0, NULL,
                                 OPEN_EXISTING, 0, NULL);
        if(!hPortHandle)
        {
            cerr << "Couldn't open port! " << lpPortName << endl;
            exit(1);
        }
        else
        {
            cout << "Port: " << lpPortName << " opened!\n";
        }
    }
    void SetupDCB()
    {
        if(!hPortHandle)
        {
            cerr << "Port not opened!\n";
            exit(1);
        }

        dcbControl.DCBlength = sizeof(dcbControl);
        dcbControl.BaudRate = BAUD_RATE;
        dcbControl.fParity = FALSE;
        dcbControl.Parity = NOPARITY;
        dcbControl.StopBits = ONESTOPBIT;
        dcbControl.ByteSize = 8;

        dcbControl.fDtrControl = DTR_CONTROL_DISABLE;
        dcbControl.fRtsControl = RTS_CONTROL_DISABLE;

        dcbControl.fOutxCtsFlow = FALSE;
        dcbControl.fOutxDsrFlow = FALSE;
        dcbControl.fDsrSensitivity = FALSE;
        dcbControl.fAbortOnError = FALSE;
        dcbControl.fOutX = FALSE;
        dcbControl.fInX = FALSE;
        dcbControl.fErrorChar = FALSE;
        dcbControl.fNull = FALSE;

        SetCommState(hPortHandle, &dcbControl);
    }
    DWORD WriteSerial(byte text[16])
    {
        DWORD sent = 0;
        //no need to pass sizeof(text)/sizeof(*text), as then
        //it would just divide by 1 (element is 'byte', which size is... 1 byte)
        //memcpy(lpWriteBuffer, sizeof(text));
        WriteFile(hPortHandle, text, sizeof(text), &sent, 0);
        return sent;
    }
};

int main()
{
    NFSUG2 nfs;
    SerialPort sp;
    sp.OpenPort("COM8", GENERIC_WRITE | GENERIC_READ);
    sp.SetupDCB();
    nfs.PrintInfo();

    byte temp[16] = {0};

    int tmpMoney = 0;
    float tmpSpeed = 0;
    int speedInt = 0;

    float tmpRev = 0;
    int revsInt = 0;

    //int tmpNitro = 0;
    //int nitroToBeSent = 0;
    //int convert[17] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};

    cout << "money (hex): " << nfs.ReadMoney() << endl;
    cout << "speed (mph): " << nfs.ReadSpeed() << endl;
    cout << "speed (km/h): " << nfs.ReadSpeed()*1.6 << endl;
    cout << "engine speed (float): " << nfs.ReadEngineSpeed() << endl;
    //cout << "nitro amount (int): " << to_string(nfs.ReadNitroAmount()) << endl;

    for(;;Sleep(10))
    {
        //SPEED:

        //read speed in mph and convert to kph:
        tmpSpeed = (nfs.ReadSpeed() * 1.6);

        //read speed in mph:
        //tmpSpeed = (nfs.ReadSpeed());

        // speed read is a bit too high? -4 for being more correct
        tmpSpeed = (tmpSpeed > 4) ? tmpSpeed - 4 : tmpSpeed;
        speedInt = (int) tmpSpeed;
        temp[1] = ( (byte) (speedInt & 0xFF) );
        temp[0] = ( (byte) ((speedInt >> 8) & 0xFF) );

        tmpRev = nfs.ReadEngineSpeed();
        revsInt = (int) tmpRev;
        temp[3] = (byte) (revsInt & 0xFF);
        temp[2] = (byte) ((revsInt >> 8) & 0xFF);

        // to show on 2x16 display, as chars, only 0-16 values will be used:
        //tmpNitro = nfs.ReadNitroAmount() / 1000;
        //nitroToBeSent = (byte) (convert[tmpNitro] & 0xFF);
        //temp[4] = nitroToBeSent;
        //cout << tmpNitro << "\t" << to_string(nitroToBeSent) << "\t" << temp[4] << endl;

        temp[5] = 0;
        temp[6] = 0;
        temp[7] = 0;

        sp.WriteSerial(temp);
        //cout << "bytes sent: 1" << sp.WriteSerial(temp) << "\t";
        //cout << " Written: " << temp << endl;

        memset(temp, 0, 16);
    }
    return 0;
}
