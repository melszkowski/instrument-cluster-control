#include <Timers.h>
#include <mcp_can.h>
#include <SPI.h>
#include <stdlib.h>

#define speedAndRpmID 0x201

// the cs pin of the version after v1.1 is default to D9
// v0.9b and v1.0 is default D10
const int SPI_CS_PIN = 3;

MCP_CAN CAN(SPI_CS_PIN);                                    // Set CS pin
Timer timer;
Timer tenthOfSec;

// GLOBAL //

byte input[8];

//speed
byte fourthByte = 0;
byte fifthByte = 50; // ostatnie 4 bity niewazne? w sumie caly bajt niewazny?!

//rev
byte zeroByte = 0;
byte firstByte = 100; // tez niewazne?

byte revAndSpeedData[8] = {zeroByte, firstByte, 255, 255, fourthBit, fifthBit, 255, 255};


int actualCarSpeed = 0;
int actualEngineSpeed = 0;
int gear;

//warning lights AND oil temp (ternary? - seems to be either normal, off or too high)
//just sending message with apropriate ID turns off few lights
//first byte is oil temp, but values to be sent - unknown
byte warningLights[8] = {140,0,0,0,0 ,0,0,0};

int whichByte = 0;
byte val = B00000001;
// END OF GLOBAL //


void setup()
{
  Serial.begin(9600);

  while (CAN_OK != CAN.begin(CAN_500KBPS, MCP_8MHz))             // init can bus : baudrate = 500k
  {
    //unable to open CAN bus, wait and try again
    delay(500);
  }
  timer.time(10);
  tenthOfSec.time(100);
  

}

void loop()
{
  // load speed and engine speed and parse it

  if (Serial.available())
  {
    Serial.readBytes(input, sizeof(input));
    
    //EOF char: (is it really needed? works without it as well)
    //nums[3] = '\0';

    // join 2 bytes from input, as its sent in 2 bytes: 8 more significant bits in index[0] byte
    // 8 less signigicant bits in index[1] byte
    actualCarSpeed = ( ((int) input[0] << 8) | (int) input[1] );
   
    actualEngineSpeed = ( ((int) input[2] << 8) | (int) input[3] );

    gear = (int) input[4];
  }

  //display on instrument cluster

  fourthByte = (byte) ((actualCarSpeed * 100 + 10000) >> 8);
  //commented, as it's irrelevant?
  //fifthByte = (byte) (( actualCarSpeed * 100 + 10000) % 256);

  zeroByte = (byte) ( actualEngineSpeed / 67.0);

  revAndSpeedData[0] = zeroByte;
  revAndSpeedData[1] = firstByte;
  revAndSpeedData[4] = fourthByte;
  revAndSpeedData[5] = fifthByte;
  if (timer.available()) {
    timer.restart();
    CAN.sendMsgBuf(speedAndRpmID, 0, 8, revAndSpeedData);
  }

  if(tenthOfSec.available()){
    tenthOfSec.restart();
    CAN.sendMsgBuf(0x420,0, 8, warningLights);
  }


  delay(1);
}

