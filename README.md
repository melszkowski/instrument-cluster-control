Mazda 6 instrument cluster control with arduino & PC
Working speedometer and rev-counter

Hobby project that consists of 2 modules: 
-- Arduino controls cluster with MCP2515 CAN controller
-- PC (written with C++) gets data from game NFS:U2 and sends it to Arduino

Video: https://youtu.be/BMulEnjfPmY